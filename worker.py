#!/usr/bin/env python
import os
import rq
import sys
import redis
import gitlab
import argparse
import Replicant
import configparser
import Replicant.WebEvents

# Gather the command line arguments we need
parser = argparse.ArgumentParser(description='Webservice worker to receive notifications from Gitlab and queue them for processing')
parser.add_argument('--config', help='Path to the configuration file to work with', required=True)
args = parser.parse_args()

# Make sure our configuration file exists
if not os.path.exists( args.config ):
	print("Unable to locate specified configuration file: %s".format(args.config))
	sys.exit(1)

# Bring the repository manager online to ensure the worker threads can make use of it
repositoryManager = Replicant.Manager( args.config )

# Connect to the upstream Gitlab server we will be working with
# To do this, we need to get our credentials and hostname first
gitlabHost  = repositoryManager.config.get('Gitlab', 'instance')
gitlabToken = repositoryManager.config.get('Gitlab', 'token')
# Now we can actually connect
gitlabServer = gitlab.Gitlab( gitlabHost, private_token=gitlabToken )

# Ensure the repository manager and gitlab server instances are available to workers
# We don't provide the repository manager instance itself because otherwise if other processes to this worker change the state on disk we will be out of sync
# Each event we process will therefore instantiate it's own repository manager instance (not as efficient, but it should be reasonably scalable)
Replicant.WebEvents.gitlabServer = gitlabServer
Replicant.WebEvents.repositoryManagerConfig = args.config

# Connect to the Redis Server
redisSocket = repositoryManager.config.get('Webservice', 'redis-socket')
redisServer = redis.Redis( unix_socket_path=redisSocket )

# Determine the name of the queue we'll be processing...
queueName   = repositoryManager.config.get('Webservice', 'queue-name')

# Start processing our work...
worker = rq.Worker( queues=[queueName], connection=redisServer )
worker.work()
