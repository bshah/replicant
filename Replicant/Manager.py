import os
import json
import time
import pygit2
import hashlib
import configparser
from .Repository import Repository

# Central class for managing the repository replica
class Manager:

	# Perform initial setup and get ready to go
	def __init__(self, configFile):
		# Make sure our configuration file is readable and exists
		if not os.path.exists( configFile ):
			raise Exception("Configuration file not found for repository specified")

		# Load the configuration file
		self.config = configparser.ConfigParser( interpolation=configparser.ExtendedInterpolation() )
		self.config.read( configFile, encoding='utf-8' )

		# Determine the paths to where we will be storing everything
		# First, the raw repository storage...
		self.repositoriesPrefix = self.config.get('Storage', 'repositories')
		# Next up, the exported symlink tree (for git-daemon, CGit and human use)
		self.exportedTreePrefix = self.config.get('Storage', 'exported-tree')

		# Now make sure those locations exist
		os.makedirs( self.repositoriesPrefix, exist_ok=True )
		os.makedirs( self.exportedTreePrefix, exist_ok=True )

		# Prepare our data structures to hold information about the repositories
		self.knownRepositories = {}

		# Load all details about the locally held repositories
		self.__loadRepositories( directory=self.repositoriesPrefix )

		# Setup our credentials keypair for use by repositories when fetching them
		self.gitlabToken = self.config.get('Gitlab', 'token')
		self.repositoryCredentials = pygit2.UserPass(username="oauth2", password=self.gitlabToken)

	# Load repositories stored within the given directory
	# It is assumed that these were originally created by Replicant or have the necessary metadata stored within them
	def __loadRepositories(self, directory):
		# Start walking over the contents of this directory
		for entry in os.scandir(directory):

			# We're not interested if this isn't a directory, in that case just ignore it
			# These shouldn't even be here
			if not entry.is_dir(follow_symlinks=False):
				continue

			# Next check to see if it's a folder containing more repositories - if so, load it up
			# Replicant always follows the structure 6b/86/6b86b273ff34fce19d6b804eff5a3f5747ada4eaa22f1d49c01e52ddb7875b4b so we check for a length of two here
			# This is a bit of an assumption, but unless something else has modified the tree it should always be true
			if len(entry.name) == 2:
				self.__loadRepositories( entry.path )
				continue

			# In theory we should now only be left with repositories, but we better do some checks first just to be sure
			# Git repositories managed by Replicant will always have a 'replicant.json' file, as well as the normal 'HEAD' file
			# We therefore use their existence as a safety check before proceeding any further...
			headPath = os.path.join( entry.path, 'HEAD' )
			jsonPath = os.path.join( entry.path, 'replicant.json' )
			if not os.path.exists( headPath ) or not os.path.exists( jsonPath ):
				continue

			# Now we know we have a repository we can load it up :)
			repository = Repository( manager=self, storage=entry.path )
			# Register it with our list of known repositories
			self.knownRepositories[ repository.hash ] = repository

	# Determines the uniquely identifying hash for a repository for this replica
	def hashForRepository( self, id ):
		# Generate a SHA-256 hash for this repository ID
		hasher = hashlib.sha256()
		hasher.update( str(id).encode('utf-8') )
		return hasher.hexdigest()

	# Retrieves a given repository using it's identification number
	def retrieve(self, id):
		# Determine the hash for this repository
		hash = self.hashForRepository( id )

		# Check to see if we have it, and if so, return it
		if hash in self.knownRepositories:
			return self.knownRepositories[hash]

		# Otherwise we have no idea
		return None

	# Register a currently unknown repository with the manager
	def register(self, metadata):
		# Calculate the hash for the repository we will be registering...
		hash = self.hashForRepository( metadata['id'] )

		# Check to see if the repository is already known to us (to prevent double ups)
		if hash in self.knownRepositories:
			return self.knownRepositories[hash]

		# Now we know it doesn't already exist, we can make it so!
		repository = Repository( manager=self, metadata=metadata )

		# Register it with our list of known repositories
		self.knownRepositories[ repository.hash ] = repository

		# Finally return the newly registered repository
		return repository

	# Mark a repository registered with us as deleted
	# This is only a 'soft deletion' and doesn't remove the repository from on disk storage, allowing for it to be restored later
	# We do record the time that the repository is deleted though to enable for future garbage collection and removal of these repositories
	def markAsDeleted(self, repository):
		# Set the necessary deleted flag on the repository
		repository.deleted    = True
		repository.deleted_on = int( time.time() )

		# Save the state of the repository to ensure the deleted flag is persisted
		return repository.save()

	# Update the exported symlink tree
	# This will remove links to any repository that has been "deleted" (but is still stored locally) and add links as needed for new repositories
	def updateExportedTree(self):
		# Start by adding in new repositories as needed (making repositories available as soon as possible)
		# To do this we go over all the repositories we know about...
		for hash, repository in self.knownRepositories.items():
			# Determine the path this repository symlink would be at
			fullPath = os.path.join( self.exportedTreePrefix, repository.path + '.git' )

			# We also need to ensure the repository we are about to try to link to exists locally
			# Otherwise there is no point adding it to the tree we are exporting
			if not os.path.exists( repository.storagePath ):
				# Sometimes this happens with repositories that haven't yet been pushed to - we'll try again later for these ones
				continue

			# Next we need to make sure it hasn't been marked as deleted - because if so it shouldn't be in the deleted tree
			if repository.deleted:
				# Skip it then
				continue

			# Now check to see if we need to create it (ie. it doesn't exist)
			if not os.path.exists( fullPath ):
				# Given it doesn't exist, we need to create it
				# Make sure the containing directory exists first though
				containingDirectory = os.path.dirname( fullPath )
				os.makedirs( containingDirectory, exist_ok=True )

				# Create the symlink!
				os.symlink( src=repository.storagePath, dst=fullPath, target_is_directory=True )
				
				# Because we just did the initial creation, nothing further is needed for this repository
				continue

			# If it already exists however, we need to ensure it is pointing to the right place!
			# This is to handle the case where one repository is moved and another has taken over it's name in short succession
			currentPath = os.path.realpath( fullPath )
			if currentPath != repository.storagePath:
				# In this case, we need to remove it as it is wrong...
				os.remove( fullPath )
				
				# And recreate it with the right details
				os.symlink( src=repository.storagePath, dst=fullPath, target_is_directory=True )
			
			# Now that we know it exists and points to the right place, we are finished
			continue

		# Step 2 of syncing it is to look for repositories which no longer exist and clean them up
		# To do this we first need to build a map of all the repositories which are supposed to exist
		repositoryPaths = [ repository.path for repository in self.knownRepositories.values() if not repository.deleted ]
		# Now we can go over all the symlinks that already exist...
		for currentPath, subdirectories, filesInFolder in os.walk( self.exportedTreePrefix, topdown=False, followlinks=False ):
			# Determine the prefix we are in, so we can look projects up in the repository path mapping
			currentPrefix = currentPath[len(self.exportedTreePrefix):]
			
			# First thing to do is to see if any of the items in this folder are repository symlinks we need to remove
			# Theoretically there should never be any other files or objects left behind
			for repository in subdirectories:
				# Determine the full on disk path
				fullPath = os.path.join( currentPath, repository )

				# Because we may be encountering an actual folder here, we should first check to see if it is a symlink
				# If it isn't, then we can ignore it as it is isn't a repository - it is part of the tree structure instead (which we'll cleanup elsewhere)
				if not os.path.islink( fullPath ):
					continue

				# Turn the repository item we have into something we can work with
				mappedPath = os.path.join( currentPrefix, repository )
				# Make sure we cut the .git off the end
				mappedPath = mappedPath[:-4]

				# Is this an item we are going to be keeping?
				if mappedPath in repositoryPaths:
					continue

				# Guess we are removing it then
				fullPath = os.path.join( currentPath, repository )
				os.remove( fullPath )

			# With that completed, now we should check to see if this directory is empty
			# This may well be the case if this project no longer exists, in which case we should cleanup to be tidy
			# Unfortunately we don't get an updated subdirectories/filesInFolder list mid-pass, so we have to do another stat for this one
			currentFolderList = os.listdir( currentPath )
			if len(currentFolderList) == 0:
				os.rmdir( currentPath )

		# With the old references cleaned up, we are done!
		return True
